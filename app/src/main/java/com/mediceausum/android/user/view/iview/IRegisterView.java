package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IRegisterPresenter;

public interface IRegisterView extends IView<IRegisterPresenter> {
    void goToLogin();
    void goToHome();
}
