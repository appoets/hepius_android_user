package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IOnBoardPresenter;

public interface IOnBoardView extends IView<IOnBoardPresenter> {

    void initSetUp();

    void gotoLogin();
}