package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.ITwilloVideoPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface ITwilloVideoView extends IView<ITwilloVideoPresenter> {
    void twiloVideoToken(Object token);
}
