package com.mediceausum.android.user.presenter;

import android.os.Bundle;

import com.mediceausum.android.user.model.dto.common.Provider;
import com.mediceausum.android.user.presenter.ipresenter.ITwilloVideoPresenter;
import com.mediceausum.android.user.view.iview.ITwilloVideoView;

/**
 * Created by Tranxit Technologies.
 */

public class TwilloVideoPresenter extends BasePresenter<ITwilloVideoView> implements ITwilloVideoPresenter {

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            Provider data = (Provider) bundle.getParcelable("data");
            //iView.setUp(data);
        }
    }

    public TwilloVideoPresenter(ITwilloVideoView iView) {
        super(iView);
    }


    @Override
    public void getTwiloToken(Object object) {
        iView.twiloVideoToken(object);
    }
}
