package com.mediceausum.android.user.presenter.ipresenter;

public interface IForgotChangePasswordPresenter extends IPresenter {
    void goToLogin();
}