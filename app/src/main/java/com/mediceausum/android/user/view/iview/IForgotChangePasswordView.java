package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IForgotChangePasswordPresenter;

public interface IForgotChangePasswordView extends IView<IForgotChangePasswordPresenter> {
                void goToLogin();
}
