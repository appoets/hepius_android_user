package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IChatPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IChatView extends IView<IChatPresenter> {
        void setUp();
}
