package com.mediceausum.android.user.view.adapter.listener;

import com.mediceausum.android.user.model.dto.response.VideoCallResponse;

public interface IVideoCallListener extends BaseRecyclerListener<VideoCallResponse> {
}
