package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IOneTimePasswordPresenter;

public interface IOneTimePasswordView extends IView<IOneTimePasswordPresenter> {
        void goToForgotChangePassword();
        void setUp();
}
