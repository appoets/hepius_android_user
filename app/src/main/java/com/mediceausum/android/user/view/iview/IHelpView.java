package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.model.dto.response.HelpResponse;
import com.mediceausum.android.user.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
