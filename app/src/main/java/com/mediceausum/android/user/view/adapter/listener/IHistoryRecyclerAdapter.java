package com.mediceausum.android.user.view.adapter.listener;

import com.mediceausum.android.user.model.dto.common.History;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryRecyclerAdapter extends BaseRecyclerListener<History> {
}
