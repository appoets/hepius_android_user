package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.model.dto.common.History;
import com.mediceausum.android.user.presenter.ipresenter.IHistoryPresenter;
import com.mediceausum.android.user.view.adapter.HistoryRecyclerAdater;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryRecyclerAdater adapter);
    void moveToChat(History data);
    void initSetUp();
}
