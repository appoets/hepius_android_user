package com.mediceausum.android.user.presenter.ipresenter;

import com.mediceausum.android.user.model.dto.request.RatingRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface IRatingPresenter extends IPresenter  {
    void postRating(RatingRequest request);
}
