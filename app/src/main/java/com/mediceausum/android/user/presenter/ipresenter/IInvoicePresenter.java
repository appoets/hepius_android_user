package com.mediceausum.android.user.presenter.ipresenter;

import com.mediceausum.android.user.model.dto.request.InvoiceRequest;
import com.mediceausum.android.user.model.dto.request.PaymentRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface IInvoicePresenter extends IPresenter {
        void requestInvoice(InvoiceRequest request);
        void requestPayment(PaymentRequest request);
}
