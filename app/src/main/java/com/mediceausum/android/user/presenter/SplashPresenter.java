package com.mediceausum.android.user.presenter;

import android.text.TextUtils;

import com.mediceausum.android.user.presenter.ipresenter.ISplashPresenter;
import com.mediceausum.android.user.model.CustomException;
import com.mediceausum.android.user.view.iview.ISplashView;

import static com.mediceausum.android.user.MyApplication.getApplicationInstance;


public class SplashPresenter extends BasePresenter<ISplashView> implements ISplashPresenter {

    private static final int SPLASH_TIME_OUT = 2000;

    public SplashPresenter(ISplashView iView) {
        super(iView);
    }


    @Override
    public boolean onCheckUserStatus() {
        return !TextUtils.isEmpty(getApplicationInstance().getAccessToken());
    }

    @Override
    public boolean hasInternet() {
        return iView.getCodeSnippet().hasNetwork();
    }

    @Override
    public void goToHome() {
        iView.gotoHome();
    }

    @Override
    public void goToLogin() {
        iView.gotoLogin();
    }

    @Override
    public void onResume() {
        iView.startTimer(SPLASH_TIME_OUT);
    }

    @Override
    public void onLogout(CustomException e) {

    }
}
