package com.mediceausum.android.user.view.adapter.listener;

import com.mediceausum.android.user.model.dto.response.ScheduleResponse;

public interface IScheduleListener extends BaseRecyclerListener<ScheduleResponse> {
}
