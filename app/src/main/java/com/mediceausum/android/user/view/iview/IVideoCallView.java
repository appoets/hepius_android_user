package com.mediceausum.android.user.view.iview;

import com.mediceausum.android.user.presenter.ipresenter.IVideoCallPresenter;
import com.mediceausum.android.user.view.adapter.NotificationRecyclerAdapter;

public interface IVideoCallView extends IView<IVideoCallPresenter> {
    void setAdapter(NotificationRecyclerAdapter adapter);
    /*void moveToChat(History data);*/
    void initSetUp();
}
