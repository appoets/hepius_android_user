package com.mediceausum.android.user.presenter.ipresenter;

import com.mediceausum.android.user.model.dto.common.videocalldata.Fcm;

public interface IMakeCallPresenter extends IPresenter {
    void sendFCMMessage(Fcm data);
}