package com.mediceausum.android.user.view.adapter.listener;

import com.mediceausum.android.user.model.dto.common.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface IProviderRecyclerAdapter extends BaseRecyclerListener<Provider> {
    void onVideoCall(Provider data);
}
